package fr.unix_experience.owncloud_sms.broadcast_receivers;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;

import org.json.JSONException;

import fr.unix_experience.owncloud_sms.engine.AndroidSmsFetcher;
import fr.unix_experience.owncloud_sms.engine.OCSMSOwnCloudClient;
import fr.unix_experience.owncloud_sms.engine.SmsEntry;
import fr.unix_experience.owncloud_sms.enums.MailboxID;
import fr.unix_experience.owncloud_sms.exceptions.OCSyncException;
import fr.unix_experience.owncloud_sms.providers.SmsDataProvider;
import fr.unix_experience.owncloud_sms.providers.SmsSendStackProvider;
import ncsmsgo.SmsBuffer;
import ncsmsgo.SmsMessage;

public class SmsSenderIntentReceiver  extends BroadcastReceiver {
    private static final int ALARM_ID = 1001;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getStringExtra("sms") != null){
            SmsMessage message = null;
            try {
                Account account = null;
                for (Account account1: AccountManager.get(context).getAccountsByType(intent.getStringExtra("account_type"))){
                    if(account1.name.equals(intent.getStringExtra("account_name"))){
                        account = account1;
                        break;
                    }
                }
                if(account == null){
                    return;
                }

                OCSMSOwnCloudClient client = new OCSMSOwnCloudClient(context, account);
                message = SmsSendStackProvider.smsMessageFromString(intent.getStringExtra("sms"));
                SmsBuffer smsBuffer = new SmsBuffer();
                SmsEntry entry = readMailBox(context, smsBuffer, message.getId(), Uri.parse(intent.getStringExtra("uri")));
                SmsSendStackProvider stackProvider = SmsSendStackProvider.getInstance(context);
                if(entry != null) {
                    stackProvider.setSent(message, entry.sent ? 3 : 5);
                    client.deleteMessage(message.getAddress(), message.getDate());
                    client.doPushRequest(smsBuffer);
                    if(entry.sent)
                        stackProvider.setSent(message, 2);
                } else {
                    stackProvider.setSent(message, 6);
                    client.deleteMessage(message.getAddress(), message.getDate());
                    smsBuffer.push(message.getId(),
                            message.getId(),
                            MailboxID.SENT.ordinal(),
                            message.getType(),
                            System.currentTimeMillis(),
                            message.getAddress(),
                            message.getCardNumber(),
                            message.getCardSlot(),
                            message.getIccId(),
                            message.getDeviceName(),
                            message.getCarrierName(),
                            message.getMessage(),
                            6,
                            "true",
                            "true") ;
                    client.doPushRequest(smsBuffer);

                }

            } catch (JSONException | OCSyncException e) {
                e.printStackTrace();
            }

        }
    }




    private SmsEntry readMailBox(Context context, SmsBuffer smsBuffer,long nc_id, Uri uri) {
        Cursor c = new SmsDataProvider(context).query(Uri.parse(MailboxID.SENT.getURI()), SmsDataProvider.messageFields, "_id = ?",  new String[] { uri.getLastPathSegment() }, null);
        SmsEntry entry = null;
        if(c!=null) {
            do {
                entry = new SmsEntry();
                for (int idx = 0; idx < c.getColumnCount(); idx++) {
                    AndroidSmsFetcher.handleProviderColumn(context, c, idx, entry);
                }
                entry.device_name = Build.MODEL;
                // Mailbox ID is required by server
                entry.mailboxId = MailboxID.SENT.ordinal();

                break;

            }
            while (c.moveToNext());
            c.close();
        }

        if(entry != null) {
            smsBuffer.push(entry.id,
                    nc_id,
                    MailboxID.SENT.ordinal(),
                    entry.type,
                    entry.date,
                    entry.address,
                    entry.card_number,
                    entry.card_slot,
                    entry.icc_id,
                    entry.device_name,
                    entry.carrier_name,
                    entry.body,
                    1,
                    "true",
                    "true") ;
            return entry;
        }
        return null;
    }

}
