package fr.unix_experience.owncloud_sms;

import android.app.Application;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Telephony;

public class CustomApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //hack to avoid being the default sms app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int wait = 0;
            if(Telephony.Sms.getDefaultSmsPackage(this) == null)
                wait = 20000;
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Telephony.Sms.getDefaultSmsPackage(CustomApplication.this) == null || !Telephony.Sms.getDefaultSmsPackage(CustomApplication.this).equals("foundation.e.esmssync"))
                            return;
                        final PackageManager manager = getPackageManager();
                        final ComponentName component = new ComponentName("foundation.e.esmssync", "fr.unix_experience.owncloud_sms.broadcast_receivers.IncomingSms");
                        manager.setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                manager.setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                            }
                        }, 30000);
                    }
                }
            ,wait);
        }



    }
}
