package fr.unix_experience.owncloud_sms.engine;

/*
 *  Copyright (c) 2014-2015, Loic Blot <loic.blot@unix-experience.fr>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import android.accounts.Account;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import foundation.e.message.IRestoreService;
import fr.unix_experience.owncloud_sms.R;
import fr.unix_experience.owncloud_sms.enums.MailboxID;
import fr.unix_experience.owncloud_sms.enums.OCSMSNotificationType;
import fr.unix_experience.owncloud_sms.notifications.OCSMSNotificationUI;
import fr.unix_experience.owncloud_sms.prefs.OCSMSSharedPrefs;
import fr.unix_experience.owncloud_sms.providers.SmsDataProvider;
import ncsmsgo.SmsMessage;
import ncsmsgo.SmsMessagesResponse;

public class AsyncSmsRestore extends AsyncTask<Void, Integer, Void> {

	private final Account mAccount;
	private final Context mContext;
	private String mMessages;

	public AsyncSmsRestore(Context context, Account account) {
		mAccount = account;
		mContext = context;
	}

	@Override
	protected Void doInBackground(Void... params) {

		if (new OCSMSSharedPrefs(mContext).showSyncNotifications()) {
			OCSMSNotificationUI.notify(mContext, mContext.getString(R.string.sync_title),
					mContext.getString(R.string.sync_inprogress), OCSMSNotificationType.SYNC.ordinal());
		}

		try {
			// This feature is only available for Android 4.4 and greater
			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT) {
				return null;
			}

			if (!new ConnectivityMonitor(mContext).isValid()) {
				Log.e(TAG, "Restore connectivity problems, aborting");
				return null;
			}

			Log.i(TAG, "Starting background recovery");
			Long start = PreferenceManager.getDefaultSharedPreferences(mContext).getLong("last_sync_restore_"+mAccount.name,0);

			OCSMSOwnCloudClient client = new OCSMSOwnCloudClient(mContext, mAccount);
			SmsDataProvider smsDataProvider = new SmsDataProvider(mContext);
			SmsMessagesResponse obj = client.retrieveSomeMessages(start, 500);
			if (obj == null) {
				Log.i(TAG, "Retrieved returns failure");
				return null;
			}
			Integer nb = 0;
			JSONArray toRestore = new JSONArray();
			while ((obj != null) && (obj.getLastID() != start)) {
				Log.i(TAG, "Retrieving messages from " + Long.toString(start)
						+ " to " + Long.toString(obj.getLastID()));
				SmsMessage message;
				while ((message = obj.getNextMessage()) != null) {
					int mbid = (int) message.getMailbox();
					if(message.getSent() != 1) //not sent, send task will handle this
						continue;
					// Ignore invalid mailbox
					if (mbid > MailboxID.ALL.getId()) {
						Log.e(TAG, "Invalid mailbox found: " + mbid);
						continue;
					}

					String address = message.getAddress();
					String body = message.getMessage();
					int type = (int) message.getType();
					if (address.isEmpty() || body.isEmpty()) {
						Log.e(TAG, "Invalid SMS message found: " + message.toString());
						continue;
					}

					MailboxID mailbox_id = MailboxID.fromInt(mbid);

					String date = Long.toString(message.getDate());
					// Ignore already existing messages
					if (smsDataProvider.messageExists(address, body, date, mailbox_id)) {
						Log.i(TAG, "Message exists");
						continue;
					}
					Log.i(TAG, "Adding message");
					JSONObject messageRestore = new JSONObject();
					messageRestore.put("address", address);
					messageRestore.put("body", body);
					messageRestore.put("date", date);
					messageRestore.put("type", type);
					messageRestore.put("mailbox", mailbox_id.getURI());
					toRestore.put(messageRestore);

					nb++;
				}

				start = obj.getLastID();
				PreferenceManager.getDefaultSharedPreferences(mContext).edit().putLong("last_sync_restore_"+mAccount.name,start).apply();
				if (!new ConnectivityMonitor(mContext).isValid()) {
					Log.e(TAG, "Restore connectivity problems, aborting");
				}
				obj = client.retrieveSomeMessages(start, 500);
			}
			mMessages = toRestore.toString();
			connectRestoreService();

			Log.i(TAG, "Finishing background recovery");
			OCSMSNotificationUI.cancel(mContext);
		} catch (IllegalStateException e) {
			OCSMSNotificationUI.notify(mContext, mContext.getString(R.string.fatal_error),
					e.getMessage(), OCSMSNotificationType.SYNC_FAILED.ordinal());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	private void connectRestoreService() {

		try {
			Intent intentService = new Intent();
			intentService.setComponent(new ComponentName("foundation.e.message", "com.moez.QKSMS.feature.service.ESmsRestoreService"));

			if (!mContext.bindService(intentService, mConnection, Context.BIND_AUTO_CREATE)) {
				Log.d(TAG, "Binding to RestoreService returned false");
				throw new IllegalStateException("Binding to RestoreService returned false");
			}
		} catch (SecurityException e) {
			Log.e(TAG, "can't bind to RestoreService, check permission in Manifest");
		}
	}

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			Log.i(TAG, "RestoreService: onServiceConnected");

			IRestoreService mService = IRestoreService.Stub.asInterface(service);
			try {
				mService.restoreMessages(mMessages);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			mContext.unbindService(this);
		}

		public void onServiceDisconnected(ComponentName className) {

		}
	};

	private static final String TAG = AsyncSmsRestore.class.getSimpleName();
}
