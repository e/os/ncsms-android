package fr.unix_experience.owncloud_sms.engine;

/*
 *  Copyright (c) 2014-2015, Loic Blot <loic.blot@unix-experience.fr>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import android.accounts.Account;
import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import fr.unix_experience.owncloud_sms.R;
import fr.unix_experience.owncloud_sms.enums.OCSMSNotificationType;
import fr.unix_experience.owncloud_sms.exceptions.OCSyncException;
import fr.unix_experience.owncloud_sms.notifications.OCSMSNotificationUI;
import fr.unix_experience.owncloud_sms.prefs.OCSMSSharedPrefs;
import ncsmsgo.SmsBuffer;

public class AsyncSmsBackup extends AsyncTask<Void, Integer, Void> {

	private final Account mAccount;
	private final Context mContext;

	public AsyncSmsBackup(Context context, Account account) {
		mAccount = account;
		mContext = context;
	}

	private Context getContext(){
		return mContext;
	}
	@Override
	protected Void doInBackground(Void... params) {

		if (new OCSMSSharedPrefs(getContext()).showSyncNotifications()) {
			OCSMSNotificationUI.notify(getContext(), getContext().getString(R.string.sync_title),
					getContext().getString(R.string.sync_inprogress), OCSMSNotificationType.SYNC.ordinal());
		}

		try {
			// This feature is only available for Android 4.4 and greater
			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT) {
				return null;
			}

			if (!new ConnectivityMonitor(getContext()).isValid()) {
				Log.e(TAG, "Restore connectivity problems, aborting");
				return null;
			}

			Log.i(TAG, "Starting background backup");
			Long start = PreferenceManager.getDefaultSharedPreferences(getContext()).getLong("last_sync_backup_"+mAccount.name,0);
			SmsBuffer smsBuffer = new SmsBuffer();
			new AndroidSmsFetcher(getContext()).bufferMessagesSinceDate(smsBuffer, start);
			try {
				OCSMSOwnCloudClient _client = new OCSMSOwnCloudClient(getContext(), mAccount);
				Log.i(TAG, "Server API version: " + _client.getServerAPIVersion());
				_client.doPushRequest(smsBuffer);
				OCSMSNotificationUI.cancel(getContext());
				PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putLong("last_sync_backup_"+mAccount.name,smsBuffer.getLastMessageDate()).apply();

				Log.i(TAG, "Finishing background backup");
			} catch (IllegalStateException e) { // Fail to read mAccount data
				OCSMSNotificationUI.notify(getContext(), getContext().getString(R.string.fatal_error),
						e.getMessage(), OCSMSNotificationType.SYNC_FAILED.ordinal());
			} catch (OCSyncException e) {
				Log.e(TAG, getContext().getString(e.getErrorId()));
				OCSMSNotificationUI.notify(getContext(), getContext().getString(R.string.fatal_error),
						e.getMessage(), OCSMSNotificationType.SYNC_FAILED.ordinal());
			}

		} catch (IllegalStateException e) {
			OCSMSNotificationUI.notify(getContext(), getContext().getString(R.string.fatal_error),
					e.getMessage(), OCSMSNotificationType.SYNC_FAILED.ordinal());
		}
		return null;
	}

	private static final String TAG = AsyncSmsBackup.class.getSimpleName();
}
