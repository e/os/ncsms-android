package fr.unix_experience.owncloud_sms.sync_adapters;

/*
 *  Copyright (c) 2014-2015, Loic Blot <loic.blot@unix-experience.fr>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import fr.unix_experience.owncloud_sms.R;
import fr.unix_experience.owncloud_sms.activities.virtual.VirtualSettingsActivity;
import fr.unix_experience.owncloud_sms.defines.DefaultPrefs;
import fr.unix_experience.owncloud_sms.engine.ASyncSMSSender;
import fr.unix_experience.owncloud_sms.engine.AsyncSmsBackup;
import fr.unix_experience.owncloud_sms.engine.AsyncSmsRestore;
import fr.unix_experience.owncloud_sms.prefs.OCSMSSharedPrefs;

class SmsSyncAdapter extends AbstractThreadedSyncAdapter {

	SmsSyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
		long syncFreq = new OCSMSSharedPrefs(getContext()).getPref().getLong("sync_frequency", DefaultPrefs.syncInterval);
		Bundle b = new Bundle();
		b.putInt("synctype", 1);
		ContentResolver.removePeriodicSync(account, getContext().getString(R.string.account_sync_authority), b);
		if (syncFreq > 0) {
			ContentResolver.addPeriodicSync(account, getContext().getString(R.string.account_sync_authority), b, syncFreq * 60);
		}
		new ASyncSMSSender.SMSSenderSyncTask(getContext(),account).execute();
		new AsyncSmsRestore(getContext(),account).execute();
		new AsyncSmsBackup(getContext(),account).execute();
	}

	private static final String TAG = SmsSyncAdapter.class.getSimpleName();
}
