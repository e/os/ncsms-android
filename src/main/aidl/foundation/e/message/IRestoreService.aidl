package foundation.e.message;


interface IRestoreService {
    oneway void restoreMessages(in String messagesJson);

}
